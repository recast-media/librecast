use ws::{connect, CloseCode};
use mdns::{Record, RecordKind};
use std::{net::IpAddr, time::Duration};
use futures_util::{pin_mut, stream::StreamExt};

#[repr(C)]
pub struct RecastClient {

}

#[repr(C)]
pub struct RecastHost {
    ip: IpAddr,
    name: String,
}

const SERVICE_NAME: &'static str = "_recast._tcp.local";

impl RecastClient {
    pub async fn search_for_recast_hosts() -> Result<Vec<RecastHost>, &'static str> {
        let mut vector = Vec::<RecastHost>::new();

        let stream = match mdns::discover::all(SERVICE_NAME, Duration::from_secs(15)) {
            Ok(stream) => stream,
            Err(_) => return Err("Error creating discovery service"),
        }.listen();

        pin_mut!(stream);

        while let Some(Ok(response)) = stream.next().await {
            let addr = response.records().next();

            if let Some(addr) = addr {
                vector.push(RecastHost {
                    ip: match addr.kind {
                        RecordKind::A(ip) => std::net::IpAddr::V4(ip),
                        RecordKind::AAAA(ip) => std::net::IpAddr::V6(ip),
                        _ => continue,
                    },
                    name: addr.name.clone(),
                });
            }
        }

        Ok(vector)
    }
}